
// Snack Alert gestion
function snackAlert(message, style = 'default') {
    M.toast({html: message, classes: style})
};

// Active tooltip
$(document).ready(function(){
    $('.tooltipped').tooltip();
});

// Active sidenav
$(document).ready(function(){
    $('.sidenav').sidenav();
});

// Active DatePicker
document.addEventListener('DOMContentLoaded', function() {
    var curentDate = new Date();
    var minDate = new Date(2017, 01, 01);
    
    var elems = document.querySelectorAll('.datepicker');
    var options = {
        'defaultDate': curentDate,
        'setDefaultDate': true,
        'minDate': minDate,
        'format': 'yyyy-mm-dd'
    }
    var instances = M.Datepicker.init(elems, options);
});

// Active Media Images
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});

// Active Select
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems);
});


// Active Modal
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
});