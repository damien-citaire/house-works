<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';

getHeader('Connexion');

$errorMessage = 'Erreur, vous ne rentrez pas les bonnes informations';

if(!empty($_POST['login']) && !empty($_POST['password'])) {
    $db = getDatabaseConnexion();
    $req = $db->prepare('SELECT * FROM `user` WHERE email = :email LIMIT 1');
    $req->execute([
        'email' => $_POST['login'],
    ]);


    if(false === $user = $req->fetchObject()) {
        getSnackAlert($errorMessage, 'error');
    } else {
        if(password_verify($_POST['password'], $user->passwd)) {
            $_SESSION['logged_in'] = true;

            if(!empty($_GET['from'])) {
                header('Location: '.$_GET['from']);
            } else {
                header('Location: ' . ADMIN_PATH);
            }

        } else {
            getSnackAlert($errorMessage, 'error');
        }
    }
}
?>

<div class="row login-row">
    <div class="col s12 m8 offset-m2 l6 offset-l3">
        <div class="card">
            <div class="card-content">
            <span class="card-title">Accéder à l'admin</span>
            <br>
                <form method="post">
                    <div class="input-field col s12">
                        <input 
                            class="validate"
                            type="email"
                            name="login"
                            class="validate"
                            id="login"
                            aria-describedby="emailHelp"
                            required
                        >
                        <label for="login">Adresse Mail</label>
                    </div>
                    <div class="input-field col s12">
                        <input 
                            class="validate"
                            type="password"
                            name="password"
                            class="validate"
                            id="password"
                            required
                        >
                        <label for="password">Mot de Passe</label>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit">
                        Se connecter
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
getChillPartial('footer');

