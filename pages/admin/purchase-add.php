<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';

checkLogin();
getHeader('Ajouter un achat');

$table = 'purchases';
$insert = '`id`, `project_id`, `shop_id`, `title`, `slug`, `created_at`, `description`, `link`, `amount`, `purchased_date`, `bill`';
$values = 'NULL, :project_id, :shop_id, :title, :slug, CURRENT_TIME(), :description, NULL, :amount, :purchased_date, :bill';
$imageFileField = 'bill';

setAddingRequest($table, $insert, $values, $imageFileField);

?>

<?php getCategorieHeader('Retour à la liste des achats', ADMIN_PURCHASES_PATH, 'red'); ?>

<div class="card">
    <?php getForm('purchases', 'Ajouter un achat') ?>
</div>

<?php
getChillPartial('footer');