<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';

checkLogin();
getHeader('Modifier la facture');

$table = 'purchases';

$id = $_GET[$table];
$db = getDatabaseConnexion();
$response = $db->query("SELECT * FROM `$table` WHERE id = " . $id);
$purchaseObject =  $response->fetchObject();

// Return current image file for bill (null if there is nothing)
// It has to bas passsed to setUpdateRequest() to prevent 
// undesired file removing on update if nothing new is given
$currentBillFile = $purchaseObject->bill;

$imageFileField = 'bill';

$values = '`title` = :title,';
$values .= '`project_id` = :project_id,';
$values .= '`shop_id` = :shop_id,';
$values .= '`description` = :description,';
$values .= '`slug` = :slug,';
$values .= '`amount` = :amount,';
$values .= '`purchased_date` = :purchased_date,';
$values .= '`bill` = :' . $imageFileField .'';


setUpdateRequest($table, $values, $imageFileField, $currentBillFile);

?>

<?php getCategorieHeader('Retour à la liste des achats', ADMIN_PURCHASES_PATH, 'red'); ?>

<div class="card">
    <?php getForm('purchases', 'Modifier un achat', $purchaseObject) ?>
</div>

<?php
getChillPartial('footer');
