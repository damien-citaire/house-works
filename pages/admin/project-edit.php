<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';

checkLogin();
getHeader('Modifier un projet');

$table = 'projects';
$values = '`title` = :title,';
$values .= '`status_id` = :status_id,';
$values .= '`description` = :description,';
$values .= '`slug` = :slug ';

$id = $_GET[$table];
$db = getDatabaseConnexion();
$response = $db->query("SELECT * FROM `$table` WHERE id = " . $id);
$projectObject =  $response->fetchObject();


setUpdateRequest($table, $values);

?>

<?php getCategorieHeader('Retour à la liste des projets', ADMIN_PROJECTS_PATH, 'red'); ?>

<div class="card">
    <?php getForm('projects', 'Modifier un projet', $projectObject) ?>
</div>

<?php
getChillPartial('footer');