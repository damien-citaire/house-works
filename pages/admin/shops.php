<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';
checkLogin();

$table = 'shops';

// Display shop gestion
$perPage = 10;
$totalPost = getTotalCategorie($table);
$pages = ceil($totalPost / $perPage);
$currentPage = 1;
if (!empty($_GET['page'])) {
    $currentPage = $_GET['page'];
}

// Adding Shop gestion
$insert = '`id`, `name`';
$values = 'NULL, :name';

getHeader('Liste des magasins', ADMIN_SHOPS_PATH);

// Check doublons in shops names
$errorMessage = null;
if (!empty($_POST)) {

    $db = getDatabaseConnexion();
    $data = $_POST;
    $postedName = $data['name'];

    $request = $db->prepare(
        "SELECT name FROM $table WHERE name='$postedName';"
    );

    $request->execute(array($data['name']));
    $result= $request->fetchAll();
    
    if(count($result) != 0) {
        $errorMessage = "Ce nom existe déjà en base, évite les doublons stp";
        getSnackAlert($errorMessage, 'error');
    } else {
        setAddingRequest($table, $insert, $values);
    };
};


?>
<div class="listing-categorie-header">
    <a class="waves-effect waves-light btn green lighten-2 modal-trigger" href="#modalShop">
        Ajouter un magasin
    </a>
</div>

<!-- Modal Structure -->
<div id="modalShop" class="modal">
<div class="modal-content">
    <h4>Ajouter un magasin</h4>
    <form method="post" enctype="multipart/form-data" id="addShop">
        <label for="name">Nom du nouveau magasin</label>
            <input
                class="validate"
                type="text"
                name="name"
                id="name"
                required
            >
    </form>
</div>
<div class="modal-footer">
    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer la modale</a>
    <button
        class="waves-effect waves-light btn"
        form="addShop"
        type="submit"
    >
        Ajouter à la liste
    </button>
</div>
</div>

<?php
getCollection($table, ADMIN_SHOPS_PATH, ADMIN_SHOPS_PATH, $perPage, $currentPage);

if($pages > 1):?>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/partials/pagination.php'; ?>
<?php endif;


getChillPartial('footer');
