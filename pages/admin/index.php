<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';
checkLogin();

$table = 'purchases';

// Get the number of project
$queryProjectTotalNumber = 'SELECT COUNT(*) AS total FROM projects';
$displayProjectTitle = 'total';
$projectTotalNumber = querySelect($queryProjectTotalNumber, $displayProjectTitle);

// Get the total amount spent
$queryTotalSpent = 'SELECT SUM(ROUND(amount, 2)) AS total FROM purchases';
$displayTotalSpentTitle = 'total';
$projectTotalSpent = querySelect($queryTotalSpent, $displayTotalSpentTitle);

// Get the number of purchase
$queryPurchaseTotalNumber = 'SELECT COUNT(*) AS total FROM purchases';
$displayPurchaseTitle = 'total';
$purchaseTotalNumber = querySelect($queryPurchaseTotalNumber, $displayPurchaseTitle);

// Get the number of shop
$queryShopTotalNumber = 'SELECT COUNT(*) AS total FROM shops';
$displayShopTitle = 'total';
$shopTotalNumber = querySelect($queryShopTotalNumber, $displayShopTitle);

// Get the most old purchase date
$queryOlderPurchaseDate = 'SELECT purchased_date FROM purchases ORDER BY purchased_date ASC LIMIT 1';
$displayOlderPurchaseDate = 'purchased_date';
$olderPurchaseDate = querySelect($queryOlderPurchaseDate, $displayOlderPurchaseDate);

// Get the less old purchase date
$queryYoungerPurchaseDate = 'SELECT purchased_date FROM purchases ORDER BY purchased_date DESC LIMIT 1';
$displayYoungerPurchaseDate = 'purchased_date';
$youngerPurchaseDate = querySelect($queryYoungerPurchaseDate, $displayYoungerPurchaseDate);


getHeader('House work');


?>

    <h4>
        Statistiques des travaux: 
    </h4>
    <br>

    <div class="stats-wrapper">
        <div class="stats">
            <strong><?php echo $projectTotalSpent;?> <span style="font-size: 70%">€</span></strong> 
            <p>Dépensés</p>
        </div>
        <span class="separator"></span>
        <div class="stats">
            <strong><?php echo $projectTotalNumber;?></strong> 
            <p>Projets</p>
        </div>
        <span class="separator"></span>
        <div class="stats">
            <strong><?php echo $purchaseTotalNumber;?></strong> 
            <p>Factures</p>
        </div>
        <span class="separator"></span>
        <div class="stats">
            <strong><?php echo $olderPurchaseDate;?></strong> 
            <p>1ère Facture</p>
        </div>
        <span class="separator"></span>
        <div class="stats">
            <strong><?php echo $youngerPurchaseDate;?></strong> 
            <p>Dernière Facture</p>
        </div>
    </div>

    <br>
    <br>

    <p>C'est pas la folie encore niveau contenu pour les stats mais j'ai vu une petite lib js sympa qui génère des graphs, je vais prendre un petit moment pour m'amuser avec.<br>
    <i>(mais + tard, là il ne me reste que 2heures avant la fin de l'exam)</i>
<?php
getChillPartial('footer');