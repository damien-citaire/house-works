<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';

$purchase = getPostBySlug($_GET['s'], 'purchases');

$imagePath = 'https://via.placeholder.com/1200/09f/fff?Text=Digital.com';
if (!empty($purchase->bill)) {
    $imagePath = IMAGES_UPLOAD_PATH . $purchase->bill; 
}

$table = 'purchases';

// Get the project Name from purchases.project_id
$projectId = $purchase->project_id;
$displayProjectTitle = 'title';
$queryProject = 'SELECT projects.title ';
$queryProject .='FROM projects ';
$queryProject .='RIGHT JOIN purchases ';
$queryProject .='ON projects.id = purchases.project_id ';
$queryProject .='WHERE purchases.project_id = ' . $projectId .' ';
$queryProject .='GROUP BY project_id;';
$projectTitle = querySelect($queryProject, $displayProjectTitle);

// Get the shop Name from purchases.shop_id
$shopId = $purchase->shop_id;
$displayShopName = 'name';
$queryShop = 'SELECT shops.name ';
$queryShop .='FROM shops ';
$queryShop .='RIGHT JOIN purchases ';
$queryShop .='ON shops.id = purchases.shop_id ';
$queryShop .='WHERE purchases.shop_id = ' . $shopId .' ';
$queryShop .='GROUP BY shop_id;';
$shopName = querySelect($queryShop, $displayShopName);

getHeader('Achat : ' . $purchase->title);

getCategorieHeader('Retour à la liste des achat', ADMIN_PURCHASES_PATH, 'red');
?>


<div class="card postDetail-card">
    <div class="card-action">
        <h4 class="postDetail-title">
            <?php echo $purchase->title;?>
        </h4>
        <a
            <?php 
            echo '<a href="' . ADMIN_PURCHASE_EDIT_PATH . '?' . 'purchases' . '=' . $purchase->id . '"';
            ?>
            class="waves-effect waves-teal btn-flat teal-text"
        >
            Modifier
        </a>

        <a
            <?php
            echo 'onclick="return confirm(\'On supprime vraiment ?\');"';
            echo '<a href="' . ADMIN_PATH . 'delete.php?a=' . $table . '&b=' . $purchase->id . '"';
            ?>
            class="waves-effect waves-red btn-flat red-text"
        >
            Supprimer
        </a>
    </div>
    <div class="card-content">
        <div class='row'>
            <div class="col s12 m12 l6">
                <div class="postDetail-baseInfo">
                    <h6 class="postDetail-amount"><strong>Montant :</strong> <?php echo $purchase->amount;?> €</h6>
                    <br>

                    <?php if ($shopName != null): ?>
                        <p><strong>Magasin :</strong> <?php echo $shopName;?><p>
                    <?php endif; ?>  

                    <?php if (!empty($purchase->purchased_date)): ?>
                        <p><strong>Acheté le :</strong> <?php echo $purchase->purchased_date;?></p>
                    <?php endif; ?>

                    <p><strong>Projet :</strong> <?php echo $projectTitle;?><p>  

                    <?php if (!empty($purchase->description)): ?>
                        <br><br>
                        <h6 class="postDetail-title">Description</h6>
                        <p><?php echo $purchase->description;?></p>
                        <br>
                    <?php endif; ?> 
                </div>
                <?php
                if (!empty($purchase->created_at)) {
                    $date = new DateTime($purchase->created_at);
                    echo "<p class='postDetail-date'>Publié le " . $date->format('d/m/y H:i') . "</p>";
                }
                ?> 
            </div>
            <div class="col s12 m12 l6 postDetail-imageColumn">
                <img
                src="<?php echo $imagePath; ?>"
                class="materialboxed z-depth-3 postDetail-image"
                alt="<?php echo $imagePath; ?>">
            </div>
        </div>
    </div>
</div>


<?php
getChillPartial('footer');