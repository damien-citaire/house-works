<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';
checkLogin();

$table = 'projects';

$perPage = 10;
$totalPost = getTotalCategorie($table);
$pages = ceil($totalPost / $perPage);
$currentPage = 1;
if (!empty($_GET['page'])) {
    $currentPage = $_GET['page'];
}

getHeader('Liste des projets', ADMIN_PROJECTS_PATH);

// Return messages from deletion
// (must be after header loading for snack js scripts)
$deleteErrorMessage = 'Erreur, suppression a pas été effectuée';
$deleteSuccessMessage = 'Succès, suppression a bien été effectuée';

if (isset($_GET['delete-success'])) {
    getSnackAlert($deleteSuccessMessage, 'success');
}

if (isset($_GET['delete-error'])) {
    getSnackAlert($deleteErrorMessage, 'error');
}

getCategorieHeader('Ajouter un projet', ADMIN_PROJECT_ADD_PATH, 'green');
getCollection($table, ADMIN_PROJECT_EDIT_PATH, ADMIN_PROJECT_DETAILS_PATH, $perPage, $currentPage);

if($pages > 1):?>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/partials/pagination.php'; ?>
<?php endif;

getChillPartial('footer');
