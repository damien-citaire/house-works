<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';

checkLogin();
getHeader('Ajouter un projet');

$table = 'projects';
$insert = '`id`, `status_id`, `title`, `slug`, `created_at`, `description`, `link`';
$values = 'NULL, :status_id, :title, :slug, CURRENT_TIME(), :description, NULL';

setAddingRequest($table, $insert, $values);

?>

<?php getCategorieHeader('Retour à la liste des projets', ADMIN_PROJECTS_PATH, 'red'); ?>

<div class="card">
    <?php getForm('projects', 'Ajouter un projet') ?>
</div>

<?php
getChillPartial('footer');
