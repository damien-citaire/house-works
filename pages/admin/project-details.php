<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';

$project = getPostBySlug($_GET['s'], 'projects');

$table = 'projects';

// relative purchases displaying :
$purchasesTable = 'purchases';
$perPage = 10;
$currentPage = 1;
$projectID = $project->id;

// Get status
$projectStatusID = $project->status_id;
$displayName = 'name';
$query = 'SELECT status.name ';
$query .='FROM status ';
$query .='RIGHT JOIN projects ';
$query .='ON status.id = projects.status_id ';
$query .='WHERE projects.status_id = ' . $projectStatusID .' ';
$query .='GROUP BY status_id;';
$projectStatus = querySelect($query, $displayName);

// Total amount calcul from purchases
$totalAmount = getTotalCategorieAmount($projectID);

getHeader('Projet : ' . $project->title);

getCategorieHeader('Retour à la liste des projets', ADMIN_PROJECTS_PATH, 'red');
?>

<div class="card postDetail-card">
    <div class="card-action">
        <h4 class="postDetail-title">
            <?php echo $project->title;?>
        </h4>
        <a
            <?php 
            echo '<a href="' . ADMIN_PROJECT_EDIT_PATH . '?' . 'projects' . '=' . $project->id . '"';
            ?>
            class="waves-effect waves-teal btn-flat teal-text"
        >
            Modifier
        </a>

        <a
            <?php
            echo 'onclick="return confirm(\'On supprime vraiment ?\');"';
            echo '<a href="' . ADMIN_PATH . 'delete.php?a=' . $table . '&b=' . $project->id . '"';
            ?>
            class="waves-effect waves-red btn-flat red-text"
        >
            Supprimer
        </a>
    </div>
    <div class="card-content">
        <div class='row'>
            <div class="col s12">
                <div class="postDetail-baseInfo">
                    <?php if($totalAmount != null) :?>
                        <h6 class="postDetail-amount"><strong>Montant total :</strong> <?php echo getTotalCategorieAmount($projectID);?>€</h6>
                    <?php endif ?>
                    <p><strong>Statut :</strong> <?php echo $projectStatus;?></p>
                    <br>
                    <?php if (!empty($project->description)): ?>
                        <h6 class="postDetail-title">Description</h6>
                        <p><?php echo $project->description;?></p>
                        <br>
                    <?php endif; ?>  
                </div>
                <?php
                if (!empty($project->created_at)) {
                    $date = new DateTime($project->created_at);
                    echo "<p class='postDetail-date'>Publié le " . $date->format('d/m/y H:i') . "</p>";
                }
                ?>
                <br>
            </div>
        </div>
        <div class='row'>
            <div class="col s12">
                <h6 class="postDetail-title">Factures associées</h6>
                <?php getCollection($purchasesTable, ADMIN_PURCHASE_EDIT_PATH, ADMIN_PURCHASE_DETAILS_PATH, $perPage, $currentPage, $projectID); ?>
            </div>
        </div>
    </div>
</div>

<?php
getChillPartial('footer');

