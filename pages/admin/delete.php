<?php
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/functions.php';
checkLogin();

$db = getDatabaseConnexion();

if(!empty($_GET['a']) && !empty($_GET['b'])) {

    $table = $_GET['a'];
    $id = $_GET['b'];

    $rows = $db->exec("DELETE FROM `$table` WHERE `$table`.`id` = ".$id);

    if($rows > 0) {
        header('Location: ' . ADMIN_PATH . $table . '.php?delete-success');
    } else {
        header('Location: ' . ADMIN_PATH . $table . '.php?delete-error');
    }
}