<?php
$lastPost = getPaginatedCategorieForShop($table, $perPage, $currentPage, $projectID);
$urlWithoutQuery = strtok($_SERVER["REQUEST_URI"], '?');

?>

<ul class="collection">
    <?php if(!empty($lastPost)) :?>
        <?php foreach ($lastPost as $post):?>
            <li class="collection-item collection-custom">
                <div class="primary">
                    <h6 class="cyan-text text-darken-2 collection-title"><?php echo $post->name; ?></h6>
                </div>
                <div class="secondary">
                    <a
                        <?php
                        echo 'onclick="return confirm(\'On supprime vraiment ?\');"';
                        echo '<a href="' . ADMIN_PATH . 'delete.php?a=' . $table . '&b=' . $post->id . '"';
                        ?>
                        class="tooltipped btn-flat btn-rounded waves-effect waves-red red-text text-darken-3"
                        data-position="left"
                        data-tooltip="Suprimer"
                    >
                        <i class="material-icons">delete</i>
                    </a>
                </div>
            </li>       
        <?php endforeach;?>
    <?php elseif(empty($lastPost)) : ?>
        <li class="collection-item collection-custom">
            <div class="primary">
                <h6 class="cyan-text text-darken-2 collection-title">Rien a afficher</h6>
                <p class="blue-grey-text text-lighten-1">
                    Revenez à l'ecran précedent ou remplissez cette catégorie
                </p>
            </div>
        </li> 
    <?php endif; ?>
</ul>

