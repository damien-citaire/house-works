<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/functions.php';

// Get status to the select
$bd = getDatabaseConnexion();
$response = $bd->query("SELECT * FROM status");
$status = $response->fetchAll(PDO::FETCH_OBJ);


?>

<form method="post" enctype="multipart/form-data">
    <div class="card-content">
        <span class="card-title"><?php echo $title ?></span>
        <div class="row">
            <div class="input-field col s8">
                <input
                    class="validate"
                    type="text"
                    name="title"
                    id="title"
                    required
                    <?php if (isset($objectRequest) && !empty($objectRequest->title)): ?>
                        value="<?php echo $objectRequest->title; ?>"
                    <?php endif; ?>
                >
                <label for="title">Titre de mon projet</label>
            </div>

                <div class="input-field col s4">
                    <select name="status_id" id="status_id">
                        <?php foreach($status as $statu):?>
                            <option 
                                <?php if(isset($objectRequest) && !empty($objectRequest->status_id) && $objectRequest->status_id == $statu->id): echo 'selected="selected"'; endif;?>
                                value="<?= $statu->id;?>"
                            ><?= $statu->name;?></option>
                        <?php endforeach;?>
                    </select>
                    <label for="status_id">Projet</label>
                </div>

            <div class="input-field col s12">
                <textarea
                    class="materialize-textarea"
                    id="description"
                    name="description"
                    required
                > <?php if (isset($objectRequest) && !empty($objectRequest->description)): ?>
                    <?php echo $objectRequest->description; ?>
                <?php endif; ?></textarea>
                <label for="description">Description du projet</label>
            </div>
        </div>
    </div>
    <div class="card-action">
        <button class="waves-effect waves-light btn" type="submit">
            Valider mon projet
        </button>
    </div>
</form>