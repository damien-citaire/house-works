<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/functions.php';

if(!isset($title)) {
    $title = "House Works";
}

// Get $currentPagePath from header import
// Add optionnal posibility to higlight tab link menu
$activePage = $currentPagePath;

// Change navBar color if session is set or not
$navbarClass = (isset($_SESSION['logged_in']) && ($isClient == null)) ? 'login nav-extended' : 'logout nav-wrapper';
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/build/style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/materialize.min.js"></script>
    <script type="text/javascript" src="/assets/js/index.js"></script>
    <title><?php echo $title; ?></title>
</head>
<body>
<header>
    <nav class="<?php echo $navbarClass ?>">
        <div class="container">
            <div class="nav-wrapper">
                <?php if(isset($_SESSION['logged_in']) && ($isClient == null)) {?>
                    <span class="prefixTitleAdmin">Admin  </span>
                    <a href="<?php echo ADMIN_PATH ?>" class="brand-logo">
                        <?php echo $title; ?>
                    </a>
                <?php } else { ?>
                    <a href="/" class="brand-logo"><?php echo $title; ?></a>
                <?php } ?>   
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <?php if(isset($_SESSION['logged_in'])) :?>
                       <li>
                            <a
                                href="<?php echo LOGOUT_FILE_PATH ?>"
                                class="tooltipped btn-floating red lighten-2"
                                data-position="bottom"
                                data-tooltip="Se deconnecter"
                                >
                                <i class="material-icons">exit_to_app</i>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(!isset($_SESSION['logged_in']) || ($isClient != null)) : ?>
                        <li>
                            <a
                                href="<?php echo ADMIN_PATH ?>"
                                class="tooltipped btn-floating"
                                data-position="bottom"
                                data-tooltip="Accéder à l'admin"
                                >
                                <i class="material-icons">admin_panel_settings</i>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a
                                href="/"
                                class="tooltipped btn-floating"
                                data-position="bottom"
                                data-tooltip="Retourner à l'accueil"
                                >
                                <i class="material-icons">home</i>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php if(isset($_SESSION['logged_in']) && ($isClient == null)) :?>
                <div class="nav-content">
                    <ul class="tabs tabs-transparent">
                        <?php tabActivePage($activePage) ?>
                    </ul>
                </div> 
                <?php endif; ?> 
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <?php if(isset($_SESSION['logged_in'])) :?>
            <li><a href="<?php echo LOGOUT_FILE_PATH ?>">Se déconnecter</a></li>
        <?php endif; ?>
        <li><a href="<?php echo ADMIN_PATH ?>">Admin</a></li>
        <li><a href="/">Home</a></li>
    </ul>
</header>  
<main class="container">


