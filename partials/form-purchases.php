<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/core/functions.php';

$bd = getDatabaseConnexion();
// Get Shops to the select
$responseShops = $bd->query("SELECT * FROM shops");
$shops = $responseShops->fetchAll(PDO::FETCH_OBJ);

// Get projects to the select
$responseProjects = $bd->query("SELECT * FROM projects");
$projects = $responseProjects->fetchAll(PDO::FETCH_OBJ);
?>

<form method="post" enctype="multipart/form-data">
    <div class="card-content">
        <span class="card-title"><?php echo $title ?></span>
        <div class="row">
            <div class="input-field col s6">
                <input
                    class="validate"
                    type="text"
                    name="title"
                    id="title"
                    required
                    <?php if (isset($objectRequest) && !empty($objectRequest->title)): ?>
                        value="<?php echo $objectRequest->title; ?>"
                    <?php endif; ?>
                >
                <label for="title">Titre de mon achat</label>
            </div>
            <div class="input-field col s6">
                <select name="project_id" id="project_id">
                    <?php foreach($projects as $project):?>
                        <option 
                            <?php if(isset($objectRequest) && !empty($objectRequest->project_id) && $objectRequest->project_id == $project->id): echo 'selected="selected"'; endif;?>
                            value="<?= $project->id;?>"
                        ><?= $project->title;?></option>
                    <?php endforeach;?>
                </select>
                <label for="project_id">Projet</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <textarea
                    class="materialize-textarea"
                    id="description"
                    name="description"
                    required
                ><?php if (isset($objectRequest) && !empty($objectRequest->description)): ?>
                        <?php echo $objectRequest->description; ?>
                <?php endif; ?></textarea>
                <label for="description">Description de l'achat</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s6">
                <input
                    class="validate"
                    type="number"
                    min="1"
                    step="any"
                    name="amount"
                    id="amount"
                    placeholder="Montant"
                    <?php if (isset($objectRequest) && !empty($objectRequest->amount)): ?>
                        value="<?php echo $objectRequest->amount; ?>"
                    <?php endif; ?>
                    required
                >
                <label for="amount">Montant</label>
            </div>
            <div class="input-field col s6">
                <select name="shop_id" id="shop_id">
                    <?php foreach($shops as $shop):?>
                    <option 
                    <?php if(isset($objectRequest) && !empty($objectRequest->shop_id) && $objectRequest->shop_id == $shop->id): echo 'selected="selected"'; endif;?>
                    value="<?= $shop->id;?>"
                    ><?= $shop->name;?></option>
                    <?php endforeach;?>
                </select>
                <label for="shop_id">Magasin</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s4">
                <input
                    class="datepicker"
                    type="text"
                    name="purchased_date"
                    id="purchased_date"
                    <?php if (isset($objectRequest) && !empty($objectRequest->purchased_date)): ?>
                        value="<?php echo $objectRequest->purchased_date; ?>"
                    <?php endif; ?>
                    required
                >
                <label for="purchased_date">Date de l'achat</label> 
            </div>
            <div class="input-field col s8">
                <div class="file-field input-field" style="margin-top: 0">
                    <div class="btn btn-flat">
                        <span>Facture <?php if (isset($objectRequest) && !empty($objectRequest->bill)): ?>Fichier déjà existant<?php endif; ?></span>
                        <input
                            type="file"
                            id="bill"
                            name="bill"
                            <?php // Optionnal required if there is already a file ?>
                            <?php if (!isset($objectRequest) && empty($objectRequest->bill)): ?>
                                required
                            <?php endif; ?>
                        >
                    </div>
                    <div class="file-path-wrapper">
                        <input
                            class="file-path validate"
                            type="text"
                            placeholder="Selectionner ma facture"
                        >
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-action">
        <button class="waves-effect waves-light btn" type="submit">
            Valider mon achat
        </button>
    </div>
</form>