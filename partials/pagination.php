<?php
$urlWithoutQuery = strtok($_SERVER["REQUEST_URI"], '?');

$prev = $currentPage - 1;
$next = $currentPage + 1;
$prevLink = $urlWithoutQuery . '?page=' . $prev;
$nextLink = $urlWithoutQuery . '?page=' . $next;

?>


<ul class="pagination">
    <li class="<?= ($currentPage == 1) ? "disabled" : "waves-effect" ?>">
        <a href="<?= ($currentPage == 1) ? '#' : $prevLink ?>">
            <i class="material-icons">chevron_left</i>
        </a>
    </li>
    <?php for ($iPage = $currentPage - 2; $iPage <= $currentPage + 2; $iPage++):
        if ($iPage > 0 && $iPage <= $pages):
            ?>
            <li class="waves-effect <?= ($currentPage == $iPage) ? "active" : "" ?>">
                <a href="<?php $_SERVER['REQUEST_URI']?>?page=<?= $iPage ?>"><?= $iPage ?></a>
            </li>
        <?php
        endif;
    endfor
    ?>
    <li class="<?= ($currentPage == $pages) ? "disabled" : "waves-effect" ?>">
        <a href="<?= ($currentPage == $pages) ? '#' : $nextLink ?>">
            <i class="material-icons">chevron_right</i>
        </a>
    </li>
</ul>
