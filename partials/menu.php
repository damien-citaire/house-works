<?php

foreach ($pages as list($pageName, $url)): ?>
    <li class="tab">
        <a
            class="<?php if($url === $activePage):?>active<?php endif;?> waves-effect waves-light"
            target="_self"
            href="<?php echo $url ?>"
        >
        <?php echo $pageName;?>
        </a>
    </li>
    
<?php endforeach;?>
