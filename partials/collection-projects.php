<?php

$lastPost = getPaginatedCategorie($table, $perPage, $currentPage, $projectID);
$urlWithoutQuery = strtok($_SERVER["REQUEST_URI"], '?');

?>

<ul class="collection">
    <?php if(!empty($lastPost)) :?>
        <?php foreach ($lastPost as $post):?>
            <?php
            // Total amount calcul from purchases
            $totalAmount = getTotalCategorieAmount($post->id);
            ?>
            <?php
            $date = date_create($post->created_at);
            ?>
            <li class="collection-item collection-custom">
                <div class="primary">
                    <a
                        <?php echo 'href="' . $detailsPath . '?s=' . $post->slug . '"'; ?>
                        class="tooltipped waves-effect"
                        data-position="right"
                        data-tooltip="Consulter en détails"
                    >
                        <p class="cyan-text text-darken-2 collection-title linked"><?php echo $post->title; ?></p>
                    </a>
                    <p class="blue-grey-text text-lighten-1 collection-text">
                    <?php if($totalAmount != null) { ?>
                        <?php echo 'Montant total pour ce projet : <br><strong>' . $totalAmount . ' €</strong>';?>
                    <?php } else { ?>
                        <?php echo 'Aucune facture associée';?>
                    <?php } ?>
                    </p>
                </div>
                <?php if($projectID == false) :?>
                    <div class="secondary">
                        <a
                            <?php 
                            echo '<a href="' . $editPath . '?' . $table . '=' . $post->id . '"';
                            ?>
                            class="tooltipped collection-button btn-flat btn-rounded waves-effect waves-teal cyan-text text-darken-2"
                            data-position="bottom"
                            data-tooltip="Editer"
                        >
                            <i class="material-icons">edit</i>
                        </a>
                        <a
                            <?php
                            echo 'onclick="return confirm(\'On supprime vraiment ?\');"';
                            echo '<a href="' . ADMIN_PATH . 'delete.php?a=' . $table . '&b=' . $post->id . '"';
                            ?>
                            class="tooltipped collection-button btn-flat btn-rounded waves-effect waves-red red-text text-darken-3"
                            data-position="bottom"
                            data-tooltip="Suprimer"
                        >
                            <i class="material-icons">delete</i>
                        </a>
                    </div>
                <?php endif ?>
            </li>       
        <?php endforeach;?>
    <?php elseif(empty($lastPost)) : ?>
        <li class="collection-item collection-custom">
            <div class="primary">
                <h6 class="cyan-text text-darken-2 collection-title">Rien a afficher</h6>
                <p class="blue-grey-text text-lighten-1">
                    Revenez à l'ecran précedent ou remplissez cette catégorie
                </p>
            </div>
        </li> 
    <?php endif; ?>
</ul>

