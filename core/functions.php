<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] .'/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/consts.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/get-partials.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/core/db-requests.php';

/**
 * Active errors displaying
 */
function getErrors() {
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
}
// getErrors();

/**
 * Check if user is logged
 */
function checkLogin(): void
{
    $currentUrl = $_SERVER['REQUEST_URI'];
    if(!isset($_SESSION['logged_in'])) {
        header('Location: ' . LOGIN_FILE_PATH . '?from='.urlencode($currentUrl));
    }
}

/**
 * Get snackbar for notification messages
 * @param string $message
 * @param string|null $color
 */
function getSnackAlert(string $message, string $color = null) {
    echo '<script type="text/javascript">snackAlert(\'' . $message . '\', \'' . $color . '\');</script>';
}

/**
 * Generate Menu with active class for pages
 * @param string|null $activePage
 * activePage add active class to highlight menu
 */
function tabActivePage(string $activePage = null){
    // Array for Admin menu
    $TabPagesAdmin = [
        ['Gérer les projets', ADMIN_PROJECTS_PATH],
        ['Gérer les achats et factures', ADMIN_PURCHASES_PATH],
        ['Gérer les magasins', ADMIN_SHOPS_PATH],
    ];
    // Load relative menu if session is set or not
    $pages = $TabPagesAdmin;
    // Get Menu html structure
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/menu.php';
    
};

/**
 * Upload image files
 * @param string $inputName
 * @return string|null
 */
function uploadFile(string $inputName): ?string
{
    if (isset($_FILES[$inputName]) && $_FILES[$inputName]["error"] == 0) {
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES[$inputName]["name"];
        $filetype = $_FILES[$inputName]["type"];
        $filesize = $_FILES[$inputName]["size"];

        //Verifie le format du fichier
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (!array_key_exists($ext, $allowed)) {
            // die("Erreur : Veuillez sélectionner un format de fichier valide.");
            return getSnackAlert('Veuillez sélectionner un format de fichier valide');
        }

        // Vérifie la taille du fichier - 5Mo maximum
        $maxsize = 5 * 1024 * 1024;
        if ($filesize > $maxsize) {
            return getSnackAlert('La taille du fichier est supérieure à la limite autorisée.');
        }

        if (in_array($filetype, $allowed)) {
            // Vérifie si le fichier existe avant de le télécharger.
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/" . $filename)) {
                $filename = rand(0,99999) . '-' . $filename;
            }

            move_uploaded_file($_FILES[$inputName]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] ."/uploads/" . $filename);
            return $filename;
        } else {
            return getSnackAlert('Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer.');
        }
    } else if(isset($_FILES[$inputName]) && $_FILES[$inputName]["error"] == 4) {
        return getSnackAlert("Aucune image n\'as été ulpoadée");
    }
    else {
        return getSnackAlert("Image Error: " . $_FILES[$inputName]["error"]);
    }
}