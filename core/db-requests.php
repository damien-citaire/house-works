<?php

/**
 * @return PDO|null
 */
function getDatabaseConnexion(): ?PDO
{
    try {
        $host = 'mysql:host=' . DB_HOST . ';dbname='. DB_NAME. ';charset=utf8';
        $db = new PDO($host, DB_USER, DB_PASSWD);
        return $db;
    } catch (Exception $e) {
        return null;
    }
}

// Alternate database connection function with error in return if 
// connection fails (it was helpfull)
/*
function getDatabaseConnexion(): ?PDO
{
    try{
        $host = 'mysql:host=' . DB_HOST . ';dbname='. DB_NAME. ';charset=utf8';
        $db = new PDO(
            $host,
            DB_USER,
            DB_PASSWD,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
        );
        die(json_encode(array('outcome' => true)));
    }
    catch(PDOException $ex){
        die(dump(array(
            'outcome' => false,
            'message' => 'Unable to connect',
            'used db host' => DB_HOST,
            'used db user' => DB_USER,
            'used db name' => DB_NAME,
            'used db password' => DB_PASSWD,
        )));
    }
}
*/

/**
 * @param $table
 * @param int $perPage
 * @param int $page
 * @param false $categorie
 * @return array
 */
function getPaginatedCategorie($table, $perPage = 10, $page = 1, $categorie = false)
{
    if(null === $db = getDatabaseConnexion()) {
        return [];
    }

    $limit = $perPage;
    $offset = ($page - 1) * $perPage;
    $sql = "SELECT * FROM `$table` ORDER BY `created_at` DESC LIMIT $offset,$limit";
    if($categorie != false) {
        $sql = "SELECT * FROM `$table` WHERE `project_id` = $categorie ORDER BY `created_at`  DESC LIMIT $offset,$limit";
    }
    $response = $db->query($sql);
    return $response->fetchAll(PDO::FETCH_OBJ);
}

/**
 * @param $table
 * @param int $perPage
 * @param int $page
 * @param false $categorie
 * @return array
 */
function getPaginatedCategorieForShop($table, $perPage = 10, $page = 1, $categorie = false)
{
    if(null === $db = getDatabaseConnexion()) {
        return [];
    }

    $limit = $perPage;
    $offset = ($page - 1) * $perPage;
    $sql = "SELECT * FROM `$table` LIMIT $offset,$limit";
    $response = $db->query($sql);
    return $response->fetchAll(PDO::FETCH_OBJ);
}

/**
 * @param $table
 * @return int
 */
function getTotalCategorie($table): int
{
    if(null === $db = getDatabaseConnexion()) {
        return 0;
    }

    $response = $db->query('SELECT COUNT(*) AS total FROM ' . $table . '');
    $result =  $response->fetch();
    return $result['total'];
}

/**
 * @param string $slug
 * @param string $table
 * @return mixed|null
 */
function getPostBySlug(string $slug, string $table)
{
    if(null === $bd = getDatabaseConnexion()) {
        return null;
    }
    $response = $bd->query("SELECT * FROM `$table` WHERE slug = '$slug' LIMIT 1");

    return $response->fetchObject();
}

/**
 * Get total of amounts from specific project categorie
 * @param $project_id
 * @return null|float
 */
function getTotalCategorieAmount($project_id)
{
    if(null === $db = getDatabaseConnexion()) {
        return 0;
    }

    $response = $db->query('SELECT SUM(ROUND(amount, 2)) AS total FROM purchases WHERE project_id = ' . $project_id . '');

    $result = $response->fetch();
    return $result['total'];
}

/**
 * @param  string $query
 * @param  string $displayResult
 * @return void
 */
function querySelect(string $query, string $displayResult)
{
    if(null === $db = getDatabaseConnexion()) {
        return 0;
    }

    $response = $db->query($query);

    $result = $response->fetch();
    return $result[$displayResult];
}

/**
 * Adding post gestion
 * @param string $table
 * @param string $insert
 * @param string $values
 * @param string|null $imageFileField
 */
function setAddingRequest(
    string $table,
    string $insert,
    string $values,
    string $imageFileField = null
) {

    $errorMessage = null;
    $successMessage = null;

    if (!empty($_POST)) {
        $slugify = new Cocur\Slugify\Slugify;

        $data = $_POST;
        if ($data['title'] != null) {
            $data['slug'] = $slugify->slugify($data['title']);
        };
        $db = getDatabaseConnexion();
        if ($imageFileField != null) {
            $data[$imageFileField] = uploadFile($imageFileField);
        };

        $request = $db->prepare(
            "INSERT INTO `$table` (
                $insert
            ) VALUES (
                $values
            );"
        );

        if($request->execute($data)) {
            $successMessage = "La requête a bien été ajoutée en base";
            return getSnackAlert($successMessage, 'success');
        } else {
            $errorMessage = "La requête n\'a pas été ajoutée en base";
            return getSnackAlert($errorMessage, 'error');
        }

    }
};

/**
 * Update post gestion
 * @param string $table
 * @param string $values
 * @param string|null $imageFileField
 * @param string|null $alreadyExistingImage
 */
function setUpdateRequest(
    string $table,
    string $values,
    string $imageFileField = null,
    string $alreadyExistingImage = null
) {

    $errorMessage = null;
    $successMessage = null;

    if (!empty($_GET[$table])) {

        $id = $_GET[$table];

        $db = getDatabaseConnexion();
        $response = $db->query("SELECT * FROM `$table` WHERE id = " . $id);

        $errorMessage = null;

        if(!empty($_POST)) {
            $slugify = new Cocur\Slugify\Slugify;

            $data = $_POST;
            $data['slug'] = $slugify->slugify($data['title']);

            // if $imageFileField is not null, process image gestion
            if ($imageFileField !== null) {
                // if the input file is empty and an image is already in place,
                // keep the already existing image
                if($alreadyExistingImage !== null && $_FILES[$imageFileField]['size'] == 0)
                {
                    $data[$imageFileField] = $alreadyExistingImage;
                }
                // If the input file is not empty, add the new image from the input
                else if ($_FILES[$imageFileField]['size'] !== 0 && $_FILES[$imageFileField]['error'] == 0)
                {
                    $data[$imageFileField] = uploadFile($imageFileField);
                }
            }

            $request = $db->prepare(
                "UPDATE `$table`
                SET $values
                WHERE `$table`.`id` = $id;"
            );

            if($request->execute($data)) {
                $successMessage = "La requête a bien été ajoutée en base";
                //return getSnackAlert($successMessage, 'success');
                // remove snack and add sleep + header location to refresh and
                // actualise informations
                sleep(1);
                header('Location: ' . $_SERVER['REQUEST_URI']);
            } else {
                $errorMessage = "La requête n\'a pas été ajoutée en base";
                return getSnackAlert($errorMessage, 'error');
            }
        }
    } else {
        header('Location: ' . ADMIN_PATH . 'index.php');
    }
}

// /**
//  * Get content from specified table
//  * @return array
//  */
// function getTableContent(string $table): array
// {
//     if(null === $bd = getDatabaseConnexion()) {
//         return [];
//     }
//     $response = $bd->query("SELECT * FROM `$table`");
//     return $response->fetchAll(PDO::FETCH_OBJ);
// }