<?php

/**
 * @param string $title
 * @param string|null $currentPagePath
 * @param string|null $isClient
 * currentPagePath is optionnal for highlight tabs menu
 */
function getHeader(string $title, string $currentPagePath = null, string $isClient = null)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';
}

/**
 * @param string $text
 * @param string $path
 * @param string $color
 */
function getCategorieHeader(string $text, string $path, string $color)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/categorie-header.php';
}

/**
 * @param string $partial
 */
function getChillPartial(string $partial)
{
    include $_SERVER['DOCUMENT_ROOT'] .'/partials/' . $partial . '.php';
}

/**
 * @param string $table
 * @param string $editPath
 * @param string $detailsPath
 * @param int $perPage
 * @param int $currentPage
 * @param int|null $projectID
 */
function getCollection(string $table, string $editPath, string $detailsPath, int $perPage, int $currentPage, int $projectID = null)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/collection-'. $table .'.php';
}

/**
 * @param string $form
 * @param string $title
 * @param string $objectRequest
 * objectRequest is optionnal to diplay current edited item in forms
 */
function getForm(string $form, string $title, object $objectRequest = null)
{
    include $_SERVER['DOCUMENT_ROOT'] .'/partials/form-' . $form . '.php';
}


