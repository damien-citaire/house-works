<?php


CONST PAGE_PATH = '/pages/';
//
// Login page path
CONST LOGIN_FILE = 'login-8536.php';
CONST LOGIN_FILE_PATH  = PAGE_PATH . LOGIN_FILE;
//
// Logout page path
CONST LOGOUT_FILE = 'logout.php';
CONST LOGOUT_FILE_PATH  = PAGE_PATH . LOGOUT_FILE;
//
// Client Pages
CONST CLIENT_PATH = PAGE_PATH . 'client/';
CONST CLIENT_PROJECTS_PATH = CLIENT_PATH . 'projects.php';
CONST CLIENT_PURCHASES_PATH = CLIENT_PATH . 'purchases.php';
//
// Admin Pages
CONST ADMIN_PATH = PAGE_PATH . 'admin/';
// Project
CONST ADMIN_PROJECT_ADD_PATH = ADMIN_PATH . "project-add.php";
CONST ADMIN_PROJECT_EDIT_PATH = ADMIN_PATH . "project-edit.php";
CONST ADMIN_PROJECT_DETAILS_PATH = ADMIN_PATH . "project-details.php";
CONST ADMIN_PROJECTS_PATH = ADMIN_PATH . "projects.php";
// Purchase
CONST ADMIN_PURCHASE_ADD_PATH = ADMIN_PATH . "purchase-add.php";
CONST ADMIN_PURCHASE_EDIT_PATH = ADMIN_PATH . "purchase-edit.php";
CONST ADMIN_PURCHASE_DETAILS_PATH = ADMIN_PATH . "purchase-details.php";
CONST ADMIN_PURCHASES_PATH = ADMIN_PATH . "purchases.php";
// Shop
CONST ADMIN_SHOPS_PATH = ADMIN_PATH . "shops.php";
//
// Upload folder
CONST IMAGES_UPLOAD_PATH = '/uploads/';
